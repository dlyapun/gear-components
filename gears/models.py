from django.db import models
from core.models import BaseModel
from django.conf import settings


class RamFrequency(BaseModel):
    value = models.IntegerField()
    name = models.CharField(max_length=255)

    def __str__(self):
        return '{} {}'.format(self.value, self.name)


class Brand(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Ram(BaseModel):
    DDR3 = 'ddr3'
    DDR4 = 'ddr4'

    MEMORY_TYPE = (
        (DDR3, 'DDR3'),
        (DDR4, 'DDR4'),
    )

    GB_2 = 'gb2'
    GB_4 = 'gb4'
    GB_8 = 'gb8'
    GB_16 = 'gb16'

    MEMORY_SIZE = (
        (GB_2, '2 Gb'),
        (GB_4, '4 Gb'),
        (GB_8, '8 Gb'),
        (GB_16, '16 Gb'),
    )

    name = models.CharField(max_length=255)
    code = models.CharField(max_length=255, blank=True, null=True)
    memory_type = models.CharField(max_length=55, choices=MEMORY_TYPE, default=DDR3)

    capacity = models.IntegerField(default=1)
    memory_size = models.CharField(max_length=55, choices=MEMORY_SIZE, default=GB_8)
    brand = models.ForeignKey(Brand, on_delete=models.CASCADE)
    frequency = models.ForeignKey(RamFrequency, on_delete=models.CASCADE)

    dual_channel_mode = models.BooleanField(default=True)
    voltage = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    price = models.DecimalField(max_digits=9, decimal_places=2, blank=True, null=True)

    class Meta:
        verbose_name = 'RAM'
        verbose_name_plural = 'RAM'
