from django.contrib import admin
from .models import Brand, Ram, RamFrequency


class RamFrequencyAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'value']

class BrandAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class RamAdmin(admin.ModelAdmin):
    list_display = ['name', 'code', 'memory_type', 'capacity', 'memory_size', 'brand', 'frequency', 'voltage', 'price']


admin.site.register(RamFrequency, RamFrequencyAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Ram, BrandAdmin)
