from django.urls import path
from blog.views import PostListView, PostDetailView


app_name = 'blog'
urlpatterns = [
    path('', PostListView.as_view(), name='post_list'),
    path('<str:post_month>/<str:post_slug>', PostDetailView.as_view(), name='post'),
]
