from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import Post


class PostListView(ListView):
    template_name = 'blog/blog_list.html'
    model = Post
    paginate_by = 10


class PostDetailView(DetailView):
    template_name = 'blog/post.html'
    model = Post
    context_object_name = 'post'
    slug_url_kwarg = 'post_slug'

    def get_object(self, post_month, post_slug):
        return self.model.objects.get(
            slug=post_slug,
            publication_datetime__icontains=f'-{post_month}-'
        )

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(**kwargs)
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
