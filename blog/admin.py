from django.contrib import admin
from django.utils.html import escape
from django.utils.safestring import mark_safe
from blog.models import Tag, PostCategory, Post, Image
from imagekit.admin import AdminThumbnail
from tinymce.widgets import TinyMCE
from django.db import models


class TagAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


class PostCategoryAdsmin(admin.ModelAdmin):
    list_dislay = ['id', 'name']


class ImageAdmin(admin.ModelAdmin):
    list_display = ['id', 'admin_thumbnail']
    admin_thumbnail = AdminThumbnail(image_field='image_thumbnail')


class PostAdmin(admin.ModelAdmin):
    list_display = ['title', 'blog_body', 'image_thumbnail', 'publication_datetime', 'slug', 'author', 'status', 'category']
    formfield_overrides = {
        models.TextField: {'widget': TinyMCE(attrs={'cols': 80, 'rows': 30})},
    }

    def blog_body(self, object):
        return mark_safe(object.body)

    def image_thumbnail(self, object):
        return mark_safe(f'<img src="{escape(object.image)}"/>') if object.image else None


admin.site.register(Tag, TagAdmin)
admin.site.register(PostCategory, PostCategoryAdsmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Post, PostAdmin)
