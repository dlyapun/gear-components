from django.db import models
from core.models import BaseModel
from django.utils.text import slugify

from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import ResizeToFill

from accounts.models import User
from django.utils.translation import gettext as _


class Tag(BaseModel):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class PostCategory(BaseModel):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name_plural = 'Post categories'

    def __str__(self):
        return self.name


class Image(BaseModel):
    BLOG_MEDIA_FOLDER = 'blog_images'

    IMAGE_SIZE_X = 600 # pixels
    IMAGE_SIZE_Y = 400 # pixels
    IMAGE_QUALITY = 100 # percent
    IMAGE_FORMAT = 'JPEG'

    THUMBNAIL_SIZE_X = 100 # pixels
    THUMBNAIL_SIZE_Y = 50 # pixels
    THUMBNAIL_QUALITY = 60 # percent
    THUMBNAIL_FORMAT = 'JPEG'

    image = ProcessedImageField(
        upload_to=BLOG_MEDIA_FOLDER,
        format=IMAGE_FORMAT,
        processors=[ResizeToFill(IMAGE_SIZE_X, IMAGE_SIZE_Y)],
        options={'quality': IMAGE_QUALITY},
        null=True,
        blank=True
    )
    image_thumbnail = ImageSpecField(
        source='image',
        processors=[ResizeToFill(THUMBNAIL_SIZE_X, THUMBNAIL_SIZE_Y)],
        format=THUMBNAIL_FORMAT,
        options={'quality': THUMBNAIL_QUALITY})

    def __str__(self):
        return self.image_thumbnail.url


class Post(BaseModel):
    POST_STATUS_CHOICES = (
        ('BKP', _('Backup')),
        ('ACT', _('Active')),
        ('DIS', _('Disabled')),
    )

    title = models.CharField(max_length=200)
    body = models.TextField()
    slug = models.SlugField(max_length=200, blank=True, unique=True)
    status = models.CharField(max_length=3, choices=POST_STATUS_CHOICES)
    publication_datetime = models.DateTimeField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    category = models.ForeignKey(PostCategory, on_delete=models.CASCADE)
    image = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        ordering = ['-publication_datetime']

    def __str__(self):
        return f'"{self.title}" by {self.author.username}'

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title) if not self.slug else self.slug
        return super().save(*args, **kwargs)

    @property
    def image_url(self):
        return self.image.image.url
